

# (01) PDO Prepare Method Example 

 public function prepare($data = array()) // type hinting ...
    {
        if (array_key_exists('customer_id', $data)) {
            $this->customer_id = $data['customer_id'];
        }
        if (array_key_exists('invoice_id', $data)) {
            $this->invoice_id = $data['invoice_id'];
        }
        if (array_key_exists('issue_date', $data)) {
            $this->issue_date = $data['issue_date'];
        }
    }


# (02) PDO Insert Query Example

try {

            $stm = $this->db_connection->prepare("INSERT INTO `invoice` (customer_id, invoice_id, issue_date, payment_date, payment_terms, item, quantity, rate, amount, description)
                                     VALUES 
                                    (:customer_id, :invoice_id, :issue_date, :payment_date, :payment_terms, :item, :quantity, :rate, :amount, :description)");

            $data =  $stm->execute(array(
                ':customer_id' => $this->customer_id,
                ':invoice_id' => $this->invoice_id,
                ':issue_date' => $this->issue_date,
                ':payment_date' => $this->payment_date,
                ':payment_terms' => $this->payment_terms,
                ':item' => $this->item,
                ':quantity' => $this->quantity,
                ':rate' => $this->rate,
                ':amount' => $this->amount,
                ':description' => $this->description

            ));
            if($data){
                header('location:create.php');
            }
        } catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }


# (03) PDO Select Query Example

 public function select($query)
    {
        $stm = $this->db_connection->query($query);
        $data = $stm->fetchAll(PDO::FETCH_CLASS, "App\Invoice\Invoice"); // Return data as an Object of Invoice Class
        return $data;
    }

# (04) PDO Delete Query Example 
    
public function delete($id)
    {
        $query = "DELETE FROM `invoice` WHERE `invoice`.`id` = ".$id;
        $result = $this->db_connection->exec($query);
        if($result){
            header('location: index.php');
        }
    }

# (05) PDO Upldate Query Example

try {

            $stm = $this->db_connection->prepare("UPDATE `invoice`   
                                                          SET `issue_date` = :issue_date,
                                                          `payment_date` = :payment_date,
                                                          `payment_terms` = :payment_terms,
                                                          `item` = :item,
                                                          `quantity` = :quantity,
                                                          `rate` = :rate,
                                                          `amount` = :amount,
                                                          `description` = :description
                                                          WHERE `invoice`.`invoice_id` = $id");


            $stm->bindParam(':issue_date', $this->issue_date, PDO::PARAM_STR);
            $stm->bindParam(':payment_date', $this->payment_date, PDO::PARAM_STR);
            $stm->bindParam(':payment_terms', $this->payment_terms, PDO::PARAM_STR);
            $stm->bindParam(':item', $this->item, PDO::PARAM_STR);
            $stm->bindParam(':quantity', $this->quantity, PDO::PARAM_STR);
            $stm->bindParam(':rate', $this->rate, PDO::PARAM_STR);
            $stm->bindParam(':amount', $this->amount, PDO::PARAM_STR);
            $stm->bindParam(':description', $this->description, PDO::PARAM_STR);
            if($stm->execute()){
                header('location:view.php?id='.$id);
            }
        } catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }