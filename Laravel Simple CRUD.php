/*
Laraval CRUD Topic

01. SQL group_concate()
02. Many to Many relatino table data insert

*/



<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\News;
class NewsController extends Controller
{
    public function create()
    {
        $data = DB::table('category')->get();
        return view('admin.news.create', compact('data'));
    }

    public function show($id)
    {
        dd($id);
    }

    public function index()
    {
       $data = DB::table('news')
                  ->select('news.*',DB::raw("group_concat(category.catName) as category"))
                  ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
                  ->leftJoin('category', 'category_news.category_id', '=', 'category.id')
                  ->groupby('news.id')
                  ->get(); 

        return view('admin.news.index', compact('data'));
    }

    public function store(Request $request)
    {
        $data = [
            'title' => $request->title,
            'description' => $request->description,
            'image' => $request->image,
            'tags' => $request->tagName
        ];
        if(News::create($data)){
           $news = DB::table('news')->orderBy('created_at', 'desc')->first();
           foreach ($request->category_id as $category_id){
               DB::table('category_news')->insert([
                   'category_id' =>  $category_id,
                   'news_id' =>  $news->id
               ]);
           }
           session()->flash('message', 'Data Inserted Successfully');
            return redirect()->back();

            /* Showing flash message
            @if(session()->has('message'))
              {{ session('message') }}
            @endif*/
        }
    }

}
